<?php

namespace Pixan\Cfdi\Models;

use Illuminate\Database\Eloquent\Model;

class Cfd extends Model
{
    //
	protected $table = "cfdi";

    protected $fillable = ['cadena_original_complemento', 'fecha_timbrado', 'sello_cfd', 'no_certificado_sat', 'sello_sat',
                            'uuid', 'emiter_certificate', 'sat_cancelled', 'last_cancellation_error_message', 'xml_filename', 'qr_filename'];
    
}
