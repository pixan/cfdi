<?php

	return [
	    'MODO_PRUEBAS_CFD'                                  => true,

	    'RUTA_XSD_VALIDACION_CFDI_3_2'						=> 'http://www.sat.gob.mx/sitio_internet/cfd/3/cfdv32.xsd',
	    'RUTA_XSLT_CADENA_ORIGINAL_3_2'						=> 'http://www.facturaporinternet.mx/resources/xslt/cadenaoriginal_3_2.xslt',
	    'RUTA_XSLT_CADENA_ORIGINAL_TFD'                     => 'http://www.facturaporinternet.mx/resources/xslt/cadenaoriginal_TFD_1_0.xslt',
	    'URL_TIMBRADO_PRUEBAS'                              => 'https://dev.facturacfdi.mx:8081/WSTimbrado/WSForcogsaService?wsdl',
	    'URL_TIMBRADO'                                      => 'https://www.facturacfdi.mx/WSTimbrado/WSForcogsaService?wsdl',
	    'URL_CANCELACION'       	    					=> 'https://www.facturacfdi.mx/WSTimbrado/WSForcogsaService?wsdl',
	    'QR_PATH'								            => public_path('assets/qr/'),
	    'XML_ROUTE'								            => public_path('assets/cfdi/'),
	    'PDF_ROUTE'                                         => public_path('assets/pdf/'),
	    'CERTIFICATES_ROUTE'                                => public_path('assets/digitalSigningCertificates/'),

        'PACKAGES_ROUTE' 									=> public_path().'/assets/pkcs/',
        'CADENA_ROUTE' 										=> public_path().'/assets/cadenas/',
        'BINS_ROUTE' 										=> public_path()."/assets/bin/",

	    'BILL_STATUS_INVOICED'								=> 1,
	    'BILL_STATUS_SAT_CANCELLED'							=> 2,

	    'USUARIO_PRUEBA'                   					=> 'pruebasWS',
	    'CONTRASENA_PRUEBA'                					=> 'pruebasWS',
	    'RAZON_SOCIAL_PRUEBAS'              				=> 'ACCEM SERVICIOS EMPRESARIALES SC',
	    'RFC_PRUEBAS'										=> 'AAA010101AAA',
	];
?>