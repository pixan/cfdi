<?php
namespace Pixan\Cfdi\Facades;

use Illuminate\Support\Facades\Facade;

class Cfdi extends Facade {

    protected static function getFacadeAccessor() { return 'cfdi'; }

}

?>