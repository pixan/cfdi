<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCfdisTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cfdi', function (Blueprint $table) {
            $table->increments('id');
            $table->string('cadena_original_complemento',500);
            $table->string('fecha_timbrado');
            $table->string('sello_cfd');
            $table->string('no_certificado_sat');
            $table->string('sello_sat');
            $table->string('uuid');
            $table->string('emiter_certificate');
            $table->tinyInteger('sat_cancelled');
            $table->string('last_cancellation_error_message');
            $table->string('xml_filename');
            $table->string('qr_filename');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('cfdi');
    }
}
