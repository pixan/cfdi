<?php

namespace Pixan\Cfdi\Classes;

use Pixan\Cfdi\Models\Cfd; 
use Validator;
use Carbon\Carbon;

class Cfdi {

    private $certificateFile;
    private $keyFile;
    private $password;
    private $serialNumber;

    public function hola()
    {
        //
        echo "hola";
    }
    
    /*
     *Set all parameters
     */
    public function config($params)
    {
    	$this->certificateFile = $params['certificateFile'];
    	$this->keyFile = $params['keyFile'];
    	$this->password = $params['password'];
        $this->serialNumber = $params['serialNumber'];

    	return true;
    }

    /**
     *Get key file
     */
    public function getCertificateFile(){
    	return $this->certificateFile;
    }

    /**
     *Set certificate file
     */
    public function setCertificateFile($fullRoute){
    	$this->certificateFile = $fullRoute;

        return true;
    }

    /*
     *Get key file
     */
    public function getKeyFile(){
    	return $this->keyFile;
    }

    /**
     *Set key file
     */
    public function setKeyFile($fullRoute){
    	$this->keyFile = $fullRoute;

        return true;
    }

    /**
     *Get certificate password
     */
    public function getPassword(){
    	return $this->password;
    }

    /**
     *Set certificate password
     */
    public function setPassword($pass){
    	$this->password = $pass;

        return true;
    }

    /**
     *Get serialNumber
     */
    public function getSerialNumber(){
        return $this->serialNumber;
    }

    /**
     *Set serialNumber
     */
    public function setSerialNumber($serialNumber){
        $this->serialNumber = $serialNumber;

        return true;
    }

    //Generates Bill PDF
    public function generatePdf($bill) {
        $pdfPath = \Config::get('cfdi.PDF_ROUTE').$bill['uuid'].'.pdf';
        $pdfView = \View::make('SAT/pdf', ["bill"=>$bill])->render();
        \File::put($pdfPath, \PDF::load($pdfView, 'letter', 'portrait')->output());
    }

    //Generates Bill QR
    public function generateQr($bill, $cfd){
        //Save qr code
        $totalPartes    = explode(".", $bill['total']);
        $enteros        = str_pad($totalPartes[0], 10, "0", STR_PAD_LEFT);
        $decimales      = str_pad($totalPartes[1], 6, "0");
        $totalQR        = $enteros.".".$decimales;
        $cadenaQR = "?re=".$bill['issuer']['rfc']."&rr=".$bill['receiver']['rfc']."&tt=".$totalQR."&id=".$cfd->uuid;
        $archivoImagen = $cfd->uuid.".png";
        $cfd->qr_filename = $archivoImagen;
        $cfd->save();
        \QrCode::format('png')->size(200)->generate($cadenaQR, \Config::get('cfdi.QR_PATH').$archivoImagen);
    }

    /**
     * Generate xml
     *
     * @paraim int billId
     * @return Redirect
     */
    public function timbrar($bill)
    {

        $validator = Validator::make($bill,[
            'emission_date' => 'required',
            'issuer' => 'required|array',
            'issuer.name' => 'required',
            'issuer.rfc' => 'required',
            'issuer.regime' => 'required',
            'issuer.cfdi_username'  => 'required',
            'issuer.cfdi_password'  => 'required',
            'issuer.tax_address' => 'required|array',
            'issuer.address' => 'required|array',
            'receiver' => 'required|array',
            'receiver.name' => 'required',
            'receiver.rfc' => 'required',
            'receiver.tax_address' => 'required|array',
            'subtotal' => 'required',
            'discount' => 'required',
            'iva_percentage' => 'required',
            'iva' => 'required',
            'total' => 'required',
            'sat_payment_method' => 'required',
            'voucher_type' => 'required',
            'payment_method' => 'required',
            'folio' => 'required',
            'series' => 'required',
            'products' => 'required|array'
        ]);

        if($validator->fails()){
            return ['message' => $validator->errors(), "status" => false];
        }

        if(!$this->certificateFile || !$this->keyFile || !$this->password || !$this->serialNumber){
            return ["message" => "No se puede facturar, no tienes un certificado activo.", "status" => false];
        }

        $simplexml = new \Simplexml;

        //Get certificate string
        $path = \Config::get('cfdi.CERTIFICATES_ROUTE');
        exec("openssl enc -base64 -in ".$path.$this->certificateFile." -out ".$path."certificado_".$bill['series']."_".$bill['folio'].".txt");
        $certificado = file_get_contents($path."certificado_".$bill['series']."_".$bill['folio'].".txt");
        $bill['certificate'] = $certificado;

        $currentTimeString = strftime("%Y-%m-%dT%T", strtotime($bill['emission_date']));
        $bill['emission_date'] = $currentTimeString;

        $bill['serialNumber'] = $this->serialNumber;
        //Construct XML
        $xmlView = \View::make('SAT/comprobanteFiscal_3_2_complemento', ["data"=>$bill])->render();

        //Start XSLT
        $xslt = new \XSLTProcessor();
        //Import STYLESHEET
        $XSL = new \DOMDocument();
        $XSL->load(asset('assets/xslt/cadenaoriginal_3_2.xslt'));
        $xslt->importStylesheet( $XSL );
        //Load xml view
        $xml_doc = new \DomDocument;
        $xml_doc->loadXML($xmlView);
        $xml_doc->saveXML();
        
        //Get cadenaoriginal
        $cadenaOriginal = $xslt->transformToXML($xml_doc);

        shell_exec("openssl pkcs8 -inform DER -in ".\Config::get('cfdi.CERTIFICATES_ROUTE').$this->keyFile." -passin pass:".$this->password." -out ".\Config::get('cfdi.PACKAGES_ROUTE').$this->keyFile.".pem");
        $key = \Config::get('cfdi.PACKAGES_ROUTE').$this->keyFile.".pem";

        $fp = fopen (\Config::get('cfdi.CADENA_ROUTE')."cadena_".$bill['series']."_".$bill['folio'].".txt", "w");
        fwrite($fp, $cadenaOriginal);
        fclose($fp);

        exec("openssl dgst -sha1 -out ".\Config::get('cfdi.BINS_ROUTE')."sign_".$bill['series']."_".$bill['folio'].".bin -sign $key ".\Config::get('cfdi.CADENA_ROUTE')."cadena_".$bill['series']."_".$bill['folio'].".txt");
        exec("openssl enc -in ".\Config::get('cfdi.BINS_ROUTE')."sign_".$bill['series']."_".$bill['folio'].".bin -a -A -out ".\Config::get('cfdi.BINS_ROUTE')."signB64_".$bill['series']."_".$bill['folio'].".txt");

        $sello = file_get_contents(\Config::get('cfdi.BINS_ROUTE')."signB64_".$bill['series']."_".$bill['folio'].".txt");
        $bill['sello'] = $sello;

        // Construct XML again with new data
        $xmlView = \View::make('SAT/comprobanteFiscal_3_2_complemento', ["data"=>$bill])->render();
        $xmlView = preg_replace('/\s+/S', " ", $xmlView);

        try {
            $clientUrl = (\Config::get('cfdi.MODO_PRUEBAS_CFD') ? \Config::get('cfdi.URL_TIMBRADO_PRUEBAS') : \Config::get('cfdi.URL_TIMBRADO'));
            $client = new \SoapClient($clientUrl);
            $params = new \stdClass();
            $params->usuario = (\Config::get('cfdi.MODO_PRUEBAS_CFD') ? \Config::get('cfdi.USUARIO_PRUEBA') : $bill['issuer']['cfdi_username']);
            $params->contrasena = (\Config::get('cfdi.MODO_PRUEBAS_CFD') ? \Config::get('cfdi.CONTRASENA_PRUEBA') : $bill['issuer']['cfdi_password']);

            $response = $client->Autenticar($params);

            if(!empty($response->return->token)){
                $token_login = $response->return->token;

                $paramsTimbrado = new \stdClass();
                $paramsTimbrado->cfd = $xmlView;
                $paramsTimbrado->token = $token_login;

                try{
                    $rTimbrado = $client->Timbrar($paramsTimbrado);

                    $codigoTimbrado = $rTimbrado->return->codigo;
                    $mensajeTimbrado = "";
                    if(isset($rTimbrado->return->mensaje)){
                        $mensajeTimbrado = $rTimbrado->return->mensaje;
                    }

                    if($codigoTimbrado!=0){
                        return ['message' => 'Error de facturación Mensaje: '.$mensajeTimbrado, "status" => false];
                    }

                    if(!empty($rTimbrado->return->cfdi)){
                        $xmlRaw = $rTimbrado->return->cfdi;
                    }
                    else {
                        return ["message" => "Error de facturación, no se pudo extraer el xml timbrado.", "status" => false];
                    }

                    // Consume stamp
                    //$entity->useStamp();

                    // Correccion Cadena Original Complemento CFDI
                    $xmlTimbrado = substr($xmlRaw, strrpos($xmlRaw, "<tfd:TimbreFiscalDigital"), strrpos($xmlRaw, "</cfdi:Complemento>")-strrpos($xmlRaw, "<tfd:TimbreFiscalDigital"));

                    // Agregar manualmente el xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
                    $xmlTimbradoXsi = "<tfd:TimbreFiscalDigital ".'xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" ';
                    $xmlTimbradoXsi .= substr($xmlTimbrado, 24);

                    //Obetner cadena original del complemento
                    //Start XSLT
                    $xsltCadenaComplemento = new \XSLTProcessor();
                    //Import STYLESHEET
                    $XSLComplemento = new \DOMDocument();
                    $XSLComplemento->load(asset('assets/xslt/cadenaoriginal_TFD_1_0.xslt'));
                    $xsltCadenaComplemento->importStylesheet($XSLComplemento);

                    //Load xml view
                    $xmlDocTimbrado = new \DomDocument;
                    $xmlDocTimbrado->loadXML($xmlTimbradoXsi);
                    $xmlDocTimbrado->saveXML();

                    //Get cadenaoriginal del complemento
                    $cadenaOriginalComplemento = $xsltCadenaComplemento->transformToXML($xmlDocTimbrado);

                    //Get cfdi info
                    $xmlObject = $simplexml->xml_parse($xmlRaw);
                    $complemento = $xmlObject['cfdi:Complemento'];
                    $timbreFiscalDigital = $complemento['tfd:TimbreFiscalDigital'];
                    $UUID = $timbreFiscalDigital['@attributes']['UUID'];
                    $fechaTimbrado = $timbreFiscalDigital['@attributes']['FechaTimbrado'];
                    $selloCFD = $timbreFiscalDigital['@attributes']['selloCFD'];
                    $noCertificadoSAT = $timbreFiscalDigital['@attributes']['noCertificadoSAT'];
                    $selloSAT = $timbreFiscalDigital['@attributes']['selloSAT'];

                    //Save xml in file
                    $xmlPath = \Config::get('cfdi.XML_ROUTE').$UUID.'.xml';
                    \File::put($xmlPath, $xmlRaw);

                    //Update bill
                    /*
                    $bill['cadena_original_complemento'] = $cadenaOriginalComplemento;
                    $bill['fecha_timbrado'] = $fechaTimbrado;
                    $bill['sello_cfd'] = $selloCFD;
                    $bill['no_certificado_sat'] = $noCertificadoSAT;
                    $bill['sello_sat'] = $selloSAT;
                    $bill['uuid'] = $UUID;
                    $bill['emiter_certificate'] = $this->serialNumber;
                    $bill['status'] = \Config::get('cfdi.BILL_STATUS_INVOICED');
                    $bill['products'] = serialize($bill['products']);
                    */

                    $cfd = Cfd::create([
                                'cadena_original_complemento' => $cadenaOriginalComplemento,
                                'fecha_timbrado' => $fechaTimbrado,
                                'sello_cfd' => $selloCFD,
                                'no_certificado_sat' => $noCertificadoSAT,
                                'sello_sat' => $selloSAT,
                                'uuid' => $UUID,
                                'emiter_certificate' => $this->serialNumber,
                                'xml_filename' => $UUID.'.xml'
                            ]);

                    if($cfd){
                        //Generate QR
                        $this->generateQr($bill, $cfd);
                        //Save PDF file
                        //$this->generatePdf($newBill); 
                        //Redirect to view
                        \Session::flash('messages', ["Factura timbrada correctamente"]);
                        return ["message" => "Success", "status" => true, 'data' => $cfd->toArray()];
                    } else {
                        return ["message" => "Error al generar la factura", "status" => false];
                    }
                }
                catch(\Exception $e){
                    dd($e);
                }
            }
            else {
                return ["message" => "Error al obtener el token para timbrar el CFDI. Usuario y contraseña de facturación pueden estar incorrectos.", "status" => false];
            }
        }
        catch(\Exception $e){
            dd($e);
        }
    }

    /***
    * Cancels a bill
    * 
    * @param  int billId
    * @return Redirect
    */
    public function cancelar($bill){
        //$bill = Bill::findOrFail($billId);
        //$entity = $bill->entity;
        //$store = $bill->store;

        $clientUrl = (\Config::get('cfdi.MODO_PRUEBAS_CFD') ? \Config::get('cfdi.URL_TIMBRADO_PRUEBAS') : \Config::get('cfdi.URL_CANCELACION'));
        $client = new \SoapClient($clientUrl);
        $params = new \stdClass();
        $params->usuario = (\Config::get('cfdi.MODO_PRUEBAS_CFD') ? \Config::get('cfdi.USUARIO_PRUEBA') : $bill['issuer']['cfdi_username']);
        $params->contrasena = (\Config::get('cfdi.MODO_PRUEBAS_CFD') ? \Config::get('cfdi.CONTRASENA_PRUEBA') : $bill['issuer']['cfdi_password']);
        $response = $client->Autenticar($params);

        if(!empty($response->return->token)){
            $cfd = Cfd::findOrFail($bill[a]);

            $certificatesPath = \Config::get('cfdi.CERTIFICATES_ROUTE');
            //$certificate = $entity->activeDigitalSigningCertificates;

            $paramsCancelacion = new \stdClass();
            $paramsCancelacion->rfcEmisor = (\Config::get('cfdi.MODO_PRUEBAS_CFD') ? \Config::get('cfdi.RFC_PRUEBAS') : $bill['issuer']['rfc']);
            $paramsCancelacion->fecha = strftime("%Y-%m-%dT%T");
            $paramsCancelacion->folios = $cfd->uuid;
            $paramsCancelacion->publicKey = file_get_contents($certificatesPath.$this->certificateFile);
            $paramsCancelacion->privateKey = file_get_contents($certificatesPath.$this->keyFile);
            $paramsCancelacion->password = $this->password;
            $paramsCancelacion->token = $response->return->token;
            $rCancelacion = $client->Cancelacion_1($paramsCancelacion);
            
            $codigo_cancelacion = 'unknown';
            $mensaje_cancelacion = null;

            if(!empty($rCancelacion->return->folios->folio->estatusUUID)){
                $codigo_cancelacion = $rCancelacion->return->folios->folio->estatusUUID;
                // Cancell successful
                $cfd->update(['sat_cancelled' => 1]);
                //Regenrate PDF to show new status
                //generatePdf($billId, $bill);
                \Session::flash('messages', ["Factura cancelada correctamente."]);
                return ["message" => "Factura cancelada correctamente.", "status" => true, 'data' => $cfd->toArray()];
            }
            else {
                if(!empty($rCancelacion->return->codEstatus)){
                    $codigo_cancelacion = $rCancelacion->return->codEstatus;
                    $mensaje_cancelacion = $rCancelacion->return->mensaje;
                    // Cancell bill in the system only
                    $cfd->last_cancellation_error_message = $mensaje_cancelacion;
                    if($cfd->save()) {
                        //Regenrate PDF to show new status
                        // generatePdf($billId, $bill);
                        return ['message' => $mensaje_cancelacion, 'status' => false];
                    }
                    else {
                        return ['message' => $cfd->errors(), 'status' => false];
                    }
                }
            }
        } else {
            return ['message' => 'No se puede cancelar la factura, usuario y contraseña erroneos.', 'status' => false];
        }
    }
}
?>