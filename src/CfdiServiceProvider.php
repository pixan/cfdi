<?php

namespace Pixan\Cfdi;

use Illuminate\Support\Facades\App;
use Illuminate\Support\ServiceProvider;

class CfdiServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
        $this->publishes([
            __DIR__ . '/Migrations' => database_path('/migrations'),
            __DIR__ . '/Config' => config_path(),
            __DIR__ . '/Public' => public_path(),
            __DIR__ . '/Views' => base_path('resources/views')
        ]);

        $this->app->make('Pixan\Cfdi\Models\Cfd');
        
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
        App::bind('cfdi', function()
        {
            return new Classes\Cfdi;
        });
    }
}
