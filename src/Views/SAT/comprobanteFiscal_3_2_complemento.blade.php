<?php echo '<?xml version="1.0" encoding="UTF-8"?>';?>
<cfdi:Comprobante
    xmlns:cfdi="http://www.sat.gob.mx/cfd/3" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.sat.gob.mx/cfd/3 http://www.sat.gob.mx/sitio_internet/cfd/3/cfdv32.xsd" version="3.2"
    LugarExpedicion="{!! (mb_detect_encoding($data['issuer']['address']['city']) == 'UTF-8') ? $data['issuer']['address']['city'] : utf8_encode($data['issuer']['address']['city']) !!} {!! (mb_detect_encoding($data['issuer']['address']['state']) == 'UTF-8') ? $data['issuer']['address']['state'] : utf8_encode($data['issuer']['address']['state']) !!}"
    certificado="{!! preg_replace('/\r|\n/', '', $data['certificate']) !!}"
    descuento="{!! (mb_detect_encoding($data['discount']) == 'UTF-8') ? $data['discount'] : utf8_encode($data['discount']) !!}"
    fecha="{!! (mb_detect_encoding($data['emission_date']) == 'UTF-8') ? $data['emission_date'] : utf8_encode($data['emission_date']) !!}"
    folio="{!! (mb_detect_encoding($data['folio']) == 'UTF-8') ? $data['folio'] : utf8_encode($data['folio']) !!}"
    formaDePago="{!! (mb_detect_encoding($data['payment_method']) == 'UTF-8') ? $data['payment_method'] : utf8_encode($data['payment_method']) !!}"
    metodoDePago="{!! $data['sat_payment_method'] !!}"
    noCertificado="{!! $data['serialNumber'] !!}"
    sello="{!! (isset($data['sello'])) ? $data['sello'] : '' !!}"
    serie="{!! (mb_detect_encoding($data['series']) == 'UTF-8') ? $data['series'] : utf8_encode($data['series']) !!}"
    subTotal="{!! (mb_detect_encoding($data['subtotal']) == 'UTF-8') ? $data['subtotal'] : utf8_encode($data['subtotal']) !!}"
    tipoDeComprobante="{!! (mb_detect_encoding($data['voucher_type']) == 'UTF-8') ? $data['voucher_type'] : utf8_encode($data['voucher_type']) !!}"
    total="{!! (mb_detect_encoding($data['total']) == 'UTF-8') ? $data['total'] : utf8_encode($data['total']) !!}">
        <cfdi:Emisor
            nombre="{!! (mb_detect_encoding($data['issuer']['name']) == 'UTF-8') ? $data['issuer']['name'] : utf8_encode($data['issuer']['name']) !!}"
            rfc="{!! (mb_detect_encoding($data['issuer']['rfc']) == 'UTF-8') ? $data['issuer']['rfc'] : utf8_encode($data['issuer']['rfc']) !!}">
            <cfdi:DomicilioFiscal
                @if(strlen($data['issuer']['tax_address']['street_name']))calle="{!! (mb_detect_encoding($data['issuer']['tax_address']['street_name']) == 'UTF-8') ? $data['issuer']['tax_address']['street_name'] : utf8_encode($data['issuer']['tax_address']['street_name']) !!}"@endif
                @if(strlen($data['issuer']['tax_address']['zip_code']))codigoPostal="{!! (mb_detect_encoding($data['issuer']['tax_address']['zip_code']) == 'UTF-8') ? $data['issuer']['tax_address']['zip_code'] : utf8_encode($data['issuer']['tax_address']['zip_code']) !!}"@endif
                @if(strlen($data['issuer']['tax_address']['community']))colonia="{!! (mb_detect_encoding($data['issuer']['tax_address']['community']) == 'UTF-8') ? $data['issuer']['tax_address']['community'] : utf8_encode($data['issuer']['tax_address']['community']) !!}"@endif
                @if(strlen($data['issuer']['tax_address']['state']))estado="{!! (mb_detect_encoding($data['issuer']['tax_address']['state']) == 'UTF-8') ? $data['issuer']['tax_address']['state'] : utf8_encode($data['issuer']['tax_address']['state']) !!}"@endif
                @if(strlen($data['issuer']['tax_address']['city']))localidad="{!! (mb_detect_encoding($data['issuer']['tax_address']['city']) == 'UTF-8') ? $data['issuer']['tax_address']['city'] : utf8_encode($data['issuer']['tax_address']['city']) !!}"@endif
                @if(strlen($data['issuer']['tax_address']['city']))municipio="{!! (mb_detect_encoding($data['issuer']['tax_address']['city']) == 'UTF-8') ? $data['issuer']['tax_address']['city'] : utf8_encode($data['issuer']['tax_address']['city']) !!}"@endif
                @if(strlen($data['issuer']['tax_address']['ext_num']))noExterior="{!! (mb_detect_encoding($data['issuer']['tax_address']['ext_num']) == 'UTF-8') ? $data['issuer']['tax_address']['ext_num'] : utf8_encode($data['issuer']['tax_address']['ext_num']) !!}"@endif
                @if(strlen($data['issuer']['tax_address']['int_num']))noInterior="{!! ($data['issuer']['tax_address']['int_num'] == 0) ? "" : (mb_detect_encoding($data['issuer']['tax_address']['int_num']) == 'UTF-8') ? $data['issuer']['tax_address']['int_num'] : utf8_encode($data['issuer']['tax_address']['int_num']) !!}"@endif
                @if(strlen($data['issuer']['tax_address']['country']))pais="{!! (mb_detect_encoding($data['issuer']['tax_address']['country']) == 'UTF-8') ? $data['issuer']['tax_address']['country'] : utf8_encode($data['issuer']['tax_address']['country']) !!}"@endif/>
            <cfdi:ExpedidoEn
                @if(strlen($data['issuer']['address']['street_name']))calle="{!! (mb_detect_encoding($data['issuer']['address']['street_name']) == 'UTF-8') ? $data['issuer']['address']['street_name'] : utf8_encode($data['issuer']['address']['street_name']) !!}"@endif
                @if(strlen($data['issuer']['address']['zip_code']))codigoPostal="{!! (mb_detect_encoding($data['issuer']['address']['zip_code']) == 'UTF-8') ? $data['issuer']['address']['zip_code'] : utf8_encode($data['issuer']['address']['zip_code']) !!}"@endif
                @if(strlen($data['issuer']['address']['community']))colonia="{!! (mb_detect_encoding($data['issuer']['address']['community']) == 'UTF-8') ? $data['issuer']['address']['community'] : utf8_encode($data['issuer']['address']['community']) !!}"@endif
                @if(strlen($data['issuer']['address']['state']))estado="{!! (mb_detect_encoding($data['issuer']['address']['state']) == 'UTF-8') ? $data['issuer']['address']['state'] : utf8_encode($data['issuer']['address']['state']) !!}"@endif
                @if(strlen($data['issuer']['address']['city']))localidad="{!! (mb_detect_encoding($data['issuer']['address']['city']) == 'UTF-8') ? $data['issuer']['address']['city'] : utf8_encode($data['issuer']['address']['city']) !!}"@endif
                @if(strlen($data['issuer']['address']['city']))municipio="{!! (mb_detect_encoding($data['issuer']['address']['city']) == 'UTF-8') ? $data['issuer']['address']['city'] : utf8_encode($data['issuer']['address']['city']) !!}"@endif
                @if(strlen($data['issuer']['address']['ext_num']))noExterior="{!! (mb_detect_encoding($data['issuer']['address']['ext_num']) == 'UTF-8') ? $data['issuer']['address']['ext_num'] : utf8_encode($data['issuer']['address']['ext_num']) !!}"@endif
                @if(strlen($data['issuer']['address']['int_num']))noInterior="{!! ($data['issuer']['address']['int_num'] == 0) ? "" : (mb_detect_encoding($data['issuer']['address']['int_num']) == 'UTF-8') ? $data['issuer']['address']['int_num'] : utf8_encode($data['issuer']['address']['int_num']) !!}"@endif
                @if(strlen($data['issuer']['address']['country']))pais="{!! (mb_detect_encoding($data['issuer']['address']['country']) == 'UTF-8') ? $data['issuer']['address']['country'] : utf8_encode($data['issuer']['address']['country']) !!}"@endif/>
            <cfdi:RegimenFiscal
                Regimen="{!! (mb_detect_encoding($data['issuer']['regime']) == 'UTF-8') ? $data['issuer']['regime'] : utf8_encode($data['issuer']['regime']) !!}"/>
        </cfdi:Emisor>

        <cfdi:Receptor
            nombre="{!! (mb_detect_encoding($data['receiver']['name']) == 'UTF-8') ? $data['receiver']['name'] : utf8_encode($data['receiver']['name']) !!}"
            rfc="{!! (mb_detect_encoding($data['receiver']['rfc']) == 'UTF-8') ? $data['receiver']['rfc'] : utf8_encode($data['receiver']['rfc']) !!}">
            <cfdi:Domicilio
                @if(strlen($data['receiver']['tax_address']['street_name']))calle="{!! (mb_detect_encoding($data['receiver']['tax_address']['street_name']) == 'UTF-8') ? $data['receiver']['tax_address']['street_name'] : utf8_encode($data['receiver']['tax_address']['street_name']) !!}"@endif
                @if(strlen($data['receiver']['tax_address']['community']))colonia="{!! (mb_detect_encoding($data['receiver']['tax_address']['community']) == 'UTF-8') ? $data['receiver']['tax_address']['community'] : utf8_encode($data['receiver']['tax_address']['community']) !!}"@endif
                @if(strlen($data['receiver']['tax_address']['state']))estado="{!! (mb_detect_encoding($data['receiver']['tax_address']['state']) == 'UTF-8') ? $data['receiver']['tax_address']['state'] : utf8_encode($data['receiver']['tax_address']['state']) !!}"@endif
                @if(strlen($data['receiver']['tax_address']['city']))municipio="{!! (mb_detect_encoding($data['receiver']['tax_address']['city']) == 'UTF-8') ? $data['receiver']['tax_address']['city'] : utf8_encode($data['receiver']['tax_address']['city']) !!}"@endif
                @if(strlen($data['receiver']['tax_address']['ext_num']))noExterior="{!! (mb_detect_encoding($data['receiver']['tax_address']['ext_num']) == 'UTF-8') ? $data['receiver']['tax_address']['ext_num'] : utf8_encode($data['receiver']['tax_address']['ext_num']) !!}"@endif
                @if(strlen($data['receiver']['tax_address']['int_num']))noInterior="{!! ($data['receiver']['tax_address']['int_num'] == 0) ? "" : (mb_detect_encoding($data['receiver']['tax_address']['int_num']) == 'UTF-8') ? $data['receiver']['tax_address']['int_num'] : utf8_encode($data['receiver']['tax_address']['int_num']) !!}"@endif
                @if(strlen($data['receiver']['tax_address']['country']))pais="{!! (mb_detect_encoding($data['receiver']['tax_address']['country']) == 'UTF-8') ? $data['receiver']['tax_address']['country'] : utf8_encode($data['receiver']['tax_address']['country']) !!}"@endif/>
        </cfdi:Receptor>

        <cfdi:Conceptos>
            @foreach($data['products'] as $product)
            <cfdi:Concepto
                cantidad="{!! (mb_detect_encoding($product['quantity']) == 'UTF-8') ? $product['quantity'] : utf8_encode($product['quantity']) !!}"
                unidad="{!! (mb_detect_encoding($product['unit']) == 'UTF-8') ? $product['unit'] : utf8_encode($product['unit']) !!}"
                descripcion="{!! (mb_detect_encoding($product['description']) == 'UTF-8') ? $product['description'] : utf8_encode($product['description']) !!}"
                valorUnitario="{!! (mb_detect_encoding($product['unit_price']) == 'UTF-8') ? $product['unit_price'] : utf8_encode($product['unit_price']) !!}"
                importe="{!! (mb_detect_encoding($product['subtotal']) == 'UTF-8') ? $product['subtotal'] : utf8_encode($product['subtotal']) !!}"/>
            @endforeach
        </cfdi:Conceptos>

        <cfdi:Impuestos
            totalImpuestosTrasladados="{!! (mb_detect_encoding($data['iva']) == 'UTF-8') ? $data['iva'] : utf8_encode($data['iva']) !!}">
            <cfdi:Traslados>
                <cfdi:Traslado importe="{!! (mb_detect_encoding($data['iva']) == 'UTF-8') ? $data['iva'] : utf8_encode($data['iva']) !!}" impuesto="IVA" tasa="{!! (mb_detect_encoding($data['iva_percentage']) == 'UTF-8') ? $data['iva_percentage'] : utf8_encode($data['iva_percentage']) !!}"/>
            </cfdi:Traslados>
        </cfdi:Impuestos>

        @if(isset($data['complemento']))
        <cfdi:Complemento>
            <tfd:TimbreFiscalDigital
                xmlns:tfd="http://www.sat.gob.mx/TimbreFiscalDigital"
                FechaTimbrado="{!! $data['complemento']['FechaTimbrado'] !!}"
                UUID="{!! $data['complemento']['UUID'] !!}"
                noCertificadoSAT="{!! $data['complemento']['noCertificadoSAT'] !!}"
                selloCFD="{!! $data['complemento']['selloCFD'] !!}"
                selloSAT="{!! $data['complemento']['selloSAT'] !!}"
                version="1.0"
                xsi:schemaLocation="http://www.sat.gob.mx/TimbreFiscalDigital http://www.sat.gob.mx/sitio_internet/TimbreFiscalDigital/TimbreFiscalDigital.xsd"/>
        </cfdi:Complemento>
        @endif

</cfdi:Comprobante>
